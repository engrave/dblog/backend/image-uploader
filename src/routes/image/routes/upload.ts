import { Request, Response } from 'express';
import { handleResponseError } from '../../../submodules/shared-library';
import { body } from 'express-validator/check';
import {uploadImageFromUrl, uploadImageFromFile} from '../../../services/imagehoster/uploadImage';

const middleware: any[] =  [
    body('url').optional().isString().isURL(),
    body('file').optional()
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {

        const { url } = req.body;
        const { file } = <any>req;

        if (!url && !file) throw new Error("Invalid request");

        const link = url ? await uploadImageFromUrl(url) : await uploadImageFromFile(file);

        return res.json( {
            success: "OK",
            link: link
        });

    }, req, res);
}

export default {
    middleware,
    handler
}
