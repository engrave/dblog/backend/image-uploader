import healthApi from "../routes/health/health.routes";
import imageApi from "../routes/image/image.routes";
import { endpointLogger } from "../submodules/shared-library/utils/logger";

function routes(app:any) {
    app.use('/health', healthApi);
    app.use('/image', endpointLogger, imageApi);
}

export default routes;
