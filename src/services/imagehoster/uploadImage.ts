import axios from 'axios';
const secrets = require('@cloudreach/docker-secrets');
const FormData = require('form-data');
import { Express } from 'express';
import {logger} from "../../submodules/shared-library";
import {createHash} from 'crypto';
const dhive = require('@hiveio/dhive')
import * as cuid from 'cuid';

const key = dhive.PrivateKey.fromString(secrets.IMAGEHOSTER_PRIVATE_KEY);
const username = secrets.IMAGEHOSTER_USERNAME;

async function uploadImageFromUrl(url: string) {
    const {data} = await axios.get(url, { responseType: 'arraybuffer'});
    const buffer = Buffer.from(data, 'binary');
    return upload(buffer, cuid());
}

async function uploadImageFromFile(file: Express.Multer.File) {
    return upload(file.buffer, file.originalname);
}

const upload = async (buffer: Buffer, filename: string)=> {
    const imageHash = createHash('sha256')
        .update('ImageSigningChallenge')
        .update(buffer)
        .digest()

    const signature = key.sign(imageHash).toString();

    logger.info(`Uploading image with hash ${imageHash.toString('hex')} and signature ${signature}`, {
        imageHash: imageHash.toString('hex'),
        signature
    })

    const form = new FormData();
    form.append('file', buffer, filename);

    const {data} = await axios.post(`https://images.hive.blog/${username}/${signature}`, form, {
        headers: {
            ...form.getHeaders(),
        }
    });

    return data.url;
}

export {
    uploadImageFromUrl,
    uploadImageFromFile
}
